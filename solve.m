% Основная решающая функция.
% days_count - количество дней, на которое составляется расписание.
% day_length - длина рабочего дня.
% const1 - количество часов, которые необходимо отработать одному рабочему.
% B - количество рабочих, необходимых в каждый час каждого дня.
% H - возможные варианты выходных.
% max_error - погрешность.
% max_iterations - максимальное число итераций.
% m1 - начальный размер задачи.
function [f, F, x, extr] = solve(days_count, day_length, const1, B, H, max_error = 0, max_iterations = Inf, m1 = 80)
  a1 = 4;
  a2 = 8;
  a3 = 12;
  A0 = zeros(day_length, 1);
  A1 = day_schedule(day_length, a1);
  A2 = day_schedule(day_length, a2, 5);
  A3 = day_schedule(day_length, a3, 7);
  F = rand_schedule(m1, days_count, const1, A0, A1, A2, A3, H);
  CTYPE = repmat("L", 1, days_count * day_length);
  CTYPE2 = repmat("S", 1, days_count + 1);
  VARTYPE2 = repmat("I", 3 * days_count, 1);
  LB2 = zeros(1, 3 * days_count);
  UB2 = repmat(Inf, 1, 3 * days_count);
  vars = m1;
  extr.iter_size = [];
  extr.iter_f = [];
  extr.max_qc = [];
  iteration = 0;
  % Ограничение на количество отработанных часов в месяц для вспомагательной задачи.
  F2 = [ones(1, days_count) * 4, ones(1, days_count) * 8, ones(1, days_count) * 12; zeros(days_count, days_count * 3)];
  % Добавляем ограничения на выбор только одного расписания на день.
  for i = 1 : days_count
    F2(i + 1, i) = 1;
    F2(i + 1, i + days_count) = 1;
    F2(i + 1, i + 2 * days_count) = 1;
  endfor
  do
    % Записываем в лог размер задачи.
    extr.iter_size = [extr.iter_size, vars];
    % Решаем основную задачу.
    [x, f, status, extra] = glpk(ones(1, vars), F, B, zeros(1, vars), repmat(Inf, 1, vars), CTYPE);
    % Записываем в лог значение целевой функции.
    extr.iter_f = [extr.iter_f, f];
    % Строим целевую функцию для вспомогательной задачи.
    lambda = extra.lambda;
    rlambda = reshape(lambda, day_length, days_count)';
    [alpha1, alpha_idx1] = max(rlambda * A1, [], 2);
    [alpha2, alpha_idx2] = max(rlambda * A2, [], 2);
    [alpha3, alpha_idx3] = max(rlambda * A3, [], 2);
    alpha = [alpha1', alpha2', alpha3'];
    % Обнуляем максимальные значения критериев качества.
    max_qc = [];
    % Обнуляем новые добавляемые столбцы.
    new_cols = [];
    % Идем по всем графикам выходных.
    for i = 1 : rows(H)
      B2 = [const1; ones(days_count, 1) - H(i, :)'];
      % Решаем вспомогательную задачу.
      [x2, f2] = glpk(alpha, F2, B2, LB2, UB2, CTYPE2, VARTYPE2, -1);
      % Запоминаем максимальное значение критерия качества.
      max_qc = [max_qc, f2];
      % Если максимальное значение критерия качества больше 1, добавляем новый столбец.
      if f2 > 1 + max_error
        new_col = [];
        for j = 1 : days_count
          if H(i, j)
            new_col = [new_col; A0];
          elseif x2(j, 1)
            new_col = [new_col; A1(:, alpha_idx1(j, 1))];
          elseif x2(j + days_count, 1)
            new_col = [new_col; A2(:, alpha_idx2(j, 1))];
          elseif x2(j + 2 * days_count, 1)
            new_col = [new_col; A3(:, alpha_idx3(j, 1))];
          endif
        endfor
        new_cols = [new_cols, new_col];
      endif
    endfor
    % Записываем в лог максимальные значения критериев качества.
    extr.max_qc = [extr.max_qc; max_qc];
    % Если количество столбцов превысило заданный порог, пытаемся удалить столбцы.
    if vars + columns(new_cols) > m1
      to_remove = find(x == 0);
      if vars + columns(new_cols) - m1 >= rows(to_remove)
        F(:, to_remove) = [];
      else
        qcs = lambda' * F;
        for i = 1 : vars + columns(new_cols) - m1
          [~, to_remove] = min(qcs);
          qcs(:, to_remove) = [];
          F(:, to_remove) = [];
        endfor
      endif
    endif
    % Добавляем новые столбцы.
    F = [F, new_cols];
    % Обновляем количество столбцов.
    vars = columns(F);
    % Увеличиваем номер итерации.
    iteration++;
  until !columns(new_cols) || iteration >= max_iterations
  % Записываем в лог количество итераций.
  extr.iterations = iteration;
  F(:, find(x == 0)) = [];
  vars = columns(F);
  [x, f] = glpk(ones(1, vars), F, B, zeros(1, vars), repmat(Inf, 1, vars), CTYPE, repmat("I", vars, 1));
  to_remove = find(x == 0);
  F(:, to_remove) = [];
  x(to_remove, :) = [];
endfunction
