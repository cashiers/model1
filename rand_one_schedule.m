% Возвращает вектор-столбец со случайным почасовым расписанием на месяц.
% days_count - количество дней в месяце.
% hours - количество часов работы в месяц для одного сотрудника.
% A0 - нулевой вектор-столбец с расписанием на сутки при количестве рабочих часов сотрудника 0.
% A1 - матрица с расписанием при количестве рабочих часов сотрудника 4.
% A2 - матрица с расписанием при количестве рабочих часов сотрудника 8.
% A3 - матрица с расписанием при количестве рабочих часов сотрудника 12.
% H - матрица с расписанием выходных дней.
function res = rand_one_schedule(days_count, hours, A0, A1, A2, A3, H)
  month = rand_month(days_count, hours / 4, H);
  res = [];
  for i = 1 : days_count
    if (month(i) == 0)
      res = [res; A0(:, int_rand(1, columns(A0)))];
    elseif (month(i) == 1)
      res = [res; A1(:, int_rand(1, columns(A1)))];
    elseif (month(i) == 2)
      res = [res; A2(:, int_rand(1, columns(A2)))];
    elseif (month(i) == 3)
      res = [res; A3(:, int_rand(1, columns(A3)))];
    endif
  endfor
endfunction
