% Возвращает случайное расписание на месяц в виде вектора-строки с числами от 0 до 3.
% days_count - количество дней в месяце.
% hours - количество часов работы в месяц для одного сотрудника, деленное на 4.
% H - матрица с расписанием выходных дней.
function res = rand_month(days_count, hours, H)
  res = repmat(3, 1, days_count) .* (1 .- H(int_rand(1, rows(H)), :));
  s = sum(res);
  while (s > hours)
    i = int_rand(1, days_count);
    if (res(i) > 0)
      res(i)--;
      s--;
    endif
  endwhile
endfunction
