% Возвращает расписание на день в виде матрицы.
% day_length - общее количество рабочих часов в сутках.
% work_length - количество рабочих часов для одного сотрудника.
% breaks - номера часов для перерывов.
function a = day_schedule(day_length, work_length, breaks = [])
  breaks_length = columns(breaks);
  work_with_breaks_length = work_length + breaks_length;
  a_length = day_length - work_with_breaks_length + 1;
  a = zeros(day_length, a_length);
  for col = 1 : a_length
    for i = 1 : work_with_breaks_length
      row = col + i - 1;
      if (any(breaks == i) == 0)
        a(row, col) = 1;
      endif
    endfor
  endfor
endfunction
